'use strict'

var fs = require('fs');
var path = require('path');
var mongoosePaginate = require('mongoose-pagination');

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function getAlbum(req, res) {
  var albumId = req.params.id;
  Album.findById(albumId).populate({path: 'artist'}).exec((err, album) => {
    if (err) {
      res.status(500).send({message: 'Error en la peticion'});
    } else {
      if (!album) {
        res.status(404).send({message: 'El Album no existe'});
      } else {
        res.status(200).send({album});
      }
    }
  });
}

function getAlbums(req, res) {
  var artistId = req.params.artist;
  if (!artistId) {
    var find = Album.find({}).sort('title');
  }
  else {
    var find = Album.find({artist: artistId}).sort('year');
  }
  find.populate({path: 'artist'}).exec((err, albums) => {
    if (err) {
      res.status(500).send({message: 'Error en la peticion'});
    } else {
      if (!albums) {
        res.status(404).send({message: 'No hay albums'});
      } else {
        res.status(200).send({albums});
      }
    }
  });
}

function saveAlbum(req, res) {
  var album = new Album();
  var params = req.body;
  album.title = params.title;
  album.description = params.description;
  album.year = params.year;
  album.image = 'null';
  album.artist = params.artist;
  album.save(album, (err, albumStored) => {
    if (err) {
      res.status(500).send({message: 'Error al guardar el Album'});
    } else {
      if (!albumStored) {
        res.status(404).send({message: 'El Album no ha sido guardado'});
      } else {
        res.status(200).send({album: albumStored});
      }
    }
  });
}

function updateAlbum(req, res) {
  var albumId = req.params.id;
  var update = req.body;
  Album.findByIdAndUpdate(albumId, update, (err, albumUpdated) => {
    if (err) {
      res.status(500).send({message: 'Error al actualizar el Album'});
    }
    else {
      if (!albumUpdated) {
        res.status(404).send({message: 'El Album no ha sido actualizado'});
      }
      else {
        res.status(200).send({album: albumUpdated});
      }
    }
  })
}

function deleteAlbum(req, res) {
  var albumId = req.params.id;
  Album.findByIdAndRemove(albumId, (err, albumRemoved) => {
    if (err) {
      res.status(500).send({message: 'Error al remover el Album'});
    } else {
      if (!albumRemoved) {
        res.status(404).send({message: 'El Album no ha sido removido'});
      } else {
        Song.find({album: albumRemoved._id}).remove((err, songRemoved) => {
          if (err) {
            res.status(500).send({message: 'Error al remover la Cancion'});
          } else {
            if (!songRemoved) {
              res.status(404).send({message: 'La cancion no ha sido removido'});
            } else {
              res.status(200).send({album: albumRemoved});
            }
          }
        });
      }
    }
  });
}

function uploadImage(req, res) {
  var albumId = req.params.id;
  var file_name = 'No subido...';

  if (req.files) {
    var file_path = req.files.image.path;
    var file_split = file_path.split('\\');
    var file_name = file_split[2];
    Album.findByIdAndUpdate(albumId, {image: file_name}, (err, albumUpdated) => {
      if (!albumUpdated) {
        res.status(404).send({message: 'No se ha podido actualizar el album'});
      }
      else {
        res.status(200).send({album: albumUpdated});
      }
    });
    console.log(file_path);
  }
  else {
    res.status(200).send({message: 'No ha subido ninguna imagen'});
  }
}

function getImageFile(req, res) {
  var imageFile = req.params.imageFile;
  var pathFile = './uploads/albums/'+imageFile;
  fs.exists(pathFile, function(exists){
    if (exists) {
      res.sendFile(path.resolve(pathFile));
    } else {
      res.status(200).send({message: 'No existe la imagen'});
    }
  })
}


module.exports = {
  getAlbum,
  getAlbums,
  saveAlbum,
  updateAlbum,
  deleteAlbum,
  uploadImage,
  getImageFile
}
